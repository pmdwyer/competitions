#!/usr/bin/env python -tt

import csv
import matplotlib.pyplot as plt

if __name__ == '__main__':
  x = list()
  y = list()
  with open('python/states.csv', 'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=' ', quotechar='|')
    plots.__next__()
    for row in plots:
      print(row)
      if len(row) > 4:
        x.append(row[0])
        y.append(row[3])


  plt.plot(x, y)
  plt.xlabel('states')
  plt.ylabel('population')
  plt.show()