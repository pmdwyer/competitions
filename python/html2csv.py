#!/usr/bin/env python -tt

from html.parser import HTMLParser

import csv

class TableParser(HTMLParser):
  def __init__(self):
    super().__init__()
    self.reset()
    self.table = list()
    self.currentRow = -1
    self.insertData = False

  def handle_starttag(self, tag, attrs):
    if tag == 'table':
      self.insertData = False
    elif tag == 'thead':
      self.insertData = False
    elif tag == 'th':
      self.insertData = True
    elif tag == 'tr':
      self.currentRow += 1
      self.table.append(list())
      self.insertData = False
    elif tag == 'td':
      self.insertData = True


  def handle_endtag(self, tag):
    self.insertData = False


  def handle_data(self, data):
    if self.insertData:
      self.table[self.currentRow].append(data.strip())


if __name__ == '__main__':
  states = open('python/states.html', "r")
  parser = TableParser()
  parser.feed(states.read())
  states.close()
  del parser.table[1]
  parser.table.pop()
  parser.table.pop()
  parser.table.pop()
  parser.table.pop()
  with open('python/states.csv', 'w', newline='') as csvfile:
    stateswriter = csv.writer(csvfile, delimiter=' ', quotechar='|')
    for row in parser.table:
      stateswriter.writerow(row)