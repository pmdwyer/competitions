import Control.Monad
import Data.List.Split
import System.Random
import Text.Printf

main :: IO ()
main =
  forever $ getLine 
  >>= return . readDice
  >>= (\xs -> genRolls (head xs) (last xs))
  >>= return . makeResult
  >>= putStrLn

readDice :: String -> [Int]
readDice = map (read :: String -> Int) . splitOn "d"

diceRoll :: Int -> IO Int
diceRoll x = getStdRandom (randomR (1, x))

genRolls :: Int -> Int -> IO [Int]
genRolls n x = sequence $ take n . repeat $ diceRoll x

makeResult :: [Int] -> String
makeResult xs = show (sum xs) ++ ": " ++ show xs