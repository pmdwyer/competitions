#!/usr/bin/python3 -tt

import re
import operator

f = open('results.txt', 'r')
o = open('groups.txt', 'w')
throwaway = f.readline()
throwaway = f.readline()
throwaway = f.readline()
throwaway = f.readline()
throwaway = f.readline()

men = list()
women = list()
menGroups = dict()
womenGroups = dict()

#867  104  CHEN CHEN                WEST LAFAYETTE IN  F 24  50:25  2:36:40.1
p = re.compile('^(\d+)\s+(\d+)\s+(\w+\s)+\s+(\w+\s)+\s+(M|F)\s+(\d+).*$')

for line in f:
    line = line.strip()
    m = p.match( line )
    if m:
        print( m.groups() )
        if ( m.group( 5 ) == 'M'):
            men.append( ( int( m.group( 2 ) ), int( m.group( 6 ) ) ) )
        elif ( m.group( 5 ) == 'F'):
            women.append( ( int( m.group( 2 ) ), int( m.group( 6 ) ) ) )

men = sorted( men, key=operator.itemgetter( 0, 1 ) )
women = sorted( women, key=operator.itemgetter( 0, 1 ) )

print( men )

group = 1
i = 0

while men[ i ][ 0 ] == men[ i + 1 ][ 0 ]:
    if i == len( men ) - 1:
        break

    menGroups[ group ] = [ men[ i ][ 1 ], men[ i + 1 ][ 1 ], 1 ]
    group += 1
    i += 1
i += 1
menGroups[ group ] = [ men[ i ][ 1 ], -1, 1 ]

print( menGroups )

done = False
while i < len( men ):
    for j in range( group ):
        if men[ i ][ 1 ] >= menGroups[ j + 1 ][ 0 ] and men[ i ][ 1 ] < menGroups[ j + 2 ][ 0 ]:
            if menGroups[ j + 1 ][ 2 ] == men[ i ][ 0 ]:
                pass
            else:
                pass
    i += 1

o.write( 'Men\n' )
o.write( 'Min Age\t\tMax Age\t\tParticipants\n')
for k in menGroups.keys():
    o.write( str( menGroups[ k ][ 0 ] ) + '\t\t' + str( menGroups[ k ][ 1 ] ) + '\t\t' + str( menGroups[ k ][ 2 ] ) + '\n' )

o.write( '\nWomen\n' )
o.write( 'Min Age\t\tMax Age\t\tParticipants\n')
for k in womenGroups.keys():
    o.write( str( womenGroups[ k ][ 0 ] ) + '\t' + str( womenGroups[ k ][ 1 ] ) + '\t' + str( womenGroups[ k ][ 2 ] ) + '\n' )

print( menGroups )
print( womenGroups )
