lend amount balance = let reserve = 100
                          newBalance = balance - amount
                      in if balance < reserve
                            then Nothing
                            else Just newBalance

foo = let a = 1
      in let b = 2
         in a + b

bar = let x = 1 in
    ((let x = "foo" in x), x)

quux a = let a = "foo" in a ++ "eek!"

lend2 amount balance = 
    if amount < reserve * 0.5
        then Just newBalance
        else Nothing
    where reserve    = 100
          newBalance = balance - amount

pluralise :: String -> [Int] -> [String]
pluralise word counts = map plural counts
    where plural 0 = "no " ++ word ++ "s"
          plural 1 = "one " ++ word
          plural n = show n ++ " " ++ word ++ "s"

lend3 amount balance
    | amount <= 0               = Nothing
    | amount > reserve * 0.5    = Nothing
    | otherwise                 = Just newBalance
    where reserve = 100
          newBalance = balance - amount

niceDrop n xs | n <= 0  = xs
niceDrop _ []           = []
niceDrop n (_:xs)       = niceDrop (n - 1) xs
