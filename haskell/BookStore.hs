data BookInfo = Book Int String [String] 
                deriving (Show)

data MagazineInfo = Magazine Int String [String] 
                    deriving (Show)

data BookReview = BookReview BookInfo CustomerID String

type CustomerID = Int
type ReviewBody = String

data BetterReview = BetterReview BookInfo CustomerID ReviewBody

type CardHolder = String
type CardNumber = String
type Address = [String]

data BillingInfo = CreditCard CardNumber CardHolder Address
                 | CashOnDelivery
                 | Invoice CustomerID
                   deriving (Show)

bookId (Book id title authors) = id

bookTitle (Book id title authors) = title

bookAuthors (Book id title authors) = authors

nicerId (Book id _ _) = id
nicerTitle (Book _ title _) = title
nicerAuthors (Book _ _ authors) = authors

data Customer = Customer {
      customerId        :: CustomerID
    , customerName      :: String
    , customerAddress   :: Address
    } deriving (Show)

customer2 = Customer {
      customerId = 271828
    , customerAddress = ["1048576 Disk Drive",
                         "Milpitas, CA 95134",
                         "USA"]
    , customerName = "Jane Q. Citizen"
    }
