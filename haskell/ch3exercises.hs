import Data.List

len' (_:xs)   = 1 + len' xs
len' []     = 0

mean' xs = (sum xs) / (fromIntegral (length xs))

toPalindrome xs = xs ++ reverse' xs

reverse' (x:xs) = reverse' xs ++ [x]
reverse' []     = []

intersperse' :: a -> [[a]] -> [a]
intersperse' y (x:xs) = x ++ [y] ++ intersperse' y xs
intersperse' _ [] = []

isPalindrome xs = (reverse' xs) == xs

sortByLen :: [[a]] -> [[a]]
sortByLen = sortBy lenComp
    where lenComp a b = compare (len' a) (len' b)

data Direction = Left | Right | Straight
