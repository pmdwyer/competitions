import Data.List

data Tree a = Node a (Tree a) (Tree a)
            | Empty
            deriving (Show)

simpleTree = Node "Parent" (Node "left child" Empty Empty) (Node "right child" Empty Empty)

data MTree a = MNode a (Maybe (MTree a)) (Maybe (MTree a))
             deriving (Show)

convolutedTree = MNode "Parent" Nothing Nothing

nodesAreSame (Node a _ _) (Node b _ _)
    | a == b        = Just a
nodesAreSame _ _    = Nothing

height Empty                    = 0
height (Node a Empty Empty)     = 1
height (Node a rchild lchild)   = 1 + maximum [(height rchild), (height lchild)]
