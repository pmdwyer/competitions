/*
    ID: pdwyer51
    LANG: C++11
    TASK: ride
*/
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main ( int argc, char* argv[] )
{
    ifstream fin( "ride.in" );
    ofstream fout( "ride.out" );
    string cname, gname;
    int cnum = 1, gnum = 1;
    fin >> cname >> gname;
    
    for ( int i = 0; i < cname.length(); ++i )
    {
        cnum = ( cnum * ( ( ( int ) cname[ i ] - 'A') + 1 ) ) % 47;
    }
    for ( int j = 0; j < gname.length(); ++j )
    {
        gnum = ( gnum * ( ( ( int ) gname[ j ] - 'A' ) + 1 ) )  % 47;
    }

    if ( cnum == gnum )
    {
        fout << "GO";
    }
    else
    {
        fout << "STAY";
    }
    fout << endl;

    return 0;
}