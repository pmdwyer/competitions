/*
    ID: pdwyer51
    LANG: C++11
    TASK: gift1
*/
#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

int main ( int argc, char* argv[] )
{
    ifstream fin( "gift1.in" );
    ofstream fout( "gift1.out" );

    int np;
    unordered_map< string, int > gifts;
    vector< string > names;
    
    fin >> np;
    for ( int i = 0; i < np; ++i )
    {
        string name;
        fin >> name;
        gifts[ name ] = 0;
        names.push_back( name );
    }

    for ( int i = 0; i < np; ++i )
    {
        string p;
        int value, numFriends;
        fin >> p >> value >> numFriends;
        if ( numFriends == 0 )
        {
            continue;
        }

        gifts[ p ] = gifts[ p ] - value + ( value % numFriends );
        for ( int j = 0; j < numFriends; ++j )
        {
            string f;
            fin >> f;
            gifts[ f ] += ( value / numFriends );
        }
    }

    for ( const auto& name : names )
    {
        fout << name << ' ' << gifts[ name ] << endl;
    }

    return 0;
}