/*
    ID: pdwyer51
    LANG: C++11
    TASK: friday
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

const int MONTHS = 12;
const int DAYS_IN_MONTH[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

bool IsLeapYear( int year )
{
    if ( year % 4 > 0 )
        return false;
    if ( year % 100 > 0 )
        return true;
    if ( year % 400 > 0 )
        return false;

    return true;
}

int main( int argc, char* argv[] )
{
    ifstream fin( "friday.in" );
    ofstream fout( "friday.out" );

    int years;
    int numDays = 0;
    vector< int > totalThirteenths( 7, 0 );

    fin >> years;
    for ( int i = 0; i < years; ++i )
    {
        for ( int m = 0; m < MONTHS; ++m )
        {
            // figure out Day and increment
            totalThirteenths[ numDays % 7 ]++;

            // add the remainder days in the month
            numDays += DAYS_IN_MONTH[ m ];

            // plus 1 if it's feburary and a leap year
            if ( m == 1 && IsLeapYear( i ) )
            {
                numDays++;
            }
        }
    }

    for ( const auto& d : totalThirteenths )
    {
        fout << d << ' ';
    }
    fout << endl;

    return 0;
}