#include <iostream>
#include <vector>
#include <unordered_map>
#include <sstream>
#include <string>

using namespace std;

struct node
{
  string name;
  unordered_map<string, string> attribs;
  vector<struct node> elems;
};

using Node = struct node;

bool matchAttribute(istream& istream, pair<string, string>& attrib)
{
  attrib.first = "";
  attrib.second = "";

  if (istream.peek() == '>')
    return false;

  string name, value;
  char eq;
  istream >> name >> eq >> value;
  value.erase(0, 1);
  if (value[value.length() - 1] == '>')
  {
    istream.putback('>');
    value.erase(value.length() - 2, 2);
  }
  else
  {
    value.erase(value.size() - 1, 1);
  }

  attrib.first = name;
  attrib.second = value;

  return true;
}

Node* matchOpenElement(istream& istream)
{
  while (isspace(istream.peek()))
    istream.get();

  if (istream.peek() != '<')
    return nullptr;

  char less;
  istream >> less;

  if (istream.peek() == '/')
  {
    istream.putback('<');
    return nullptr;
  }

  string name;
  Node* n = new Node;
  istream >> n->name;

  pair<string, string> attrib;
  while (matchAttribute(istream, attrib))
  {
    n->attribs[attrib.first] = attrib.second;
  }

  char greater;
  istream >> greater;

  return n;
}

void matchCloseElement(istream& istream)
{
  char less, slash, greater;
  istream >> less >> slash;
  while (istream.peek() != '>')
    istream.get();
  istream >> greater;
}

Node* matchElement(istream& istream)
{
  Node* currElem = matchOpenElement(istream);
  
  if (currElem)
  {
    Node* child;
    while ((child = matchElement(istream)) != nullptr)
    {
      currElem->elems.push_back(*child);
    }
    matchCloseElement(istream);
  }

  return currElem;
}

Node* parse(istream& istream)
{
  Node* node = new Node;
  node->name = "hrml root";

  Node *child;
  while ((child = matchElement(istream)) != nullptr)
  {
    node->elems.push_back(*child);
  }

  return node;
}

pair<vector<string>, string> parseQuery(string line)
{
  pair<vector<string>, string> q;
  int tilde = line.find_last_of("~");
  q.second = line.substr(tilde + 1);
  line.erase(tilde);

  string nodename;
  stringstream ss(line);
  while (getline(ss, nodename, '.'))
    q.first.push_back(nodename);

  return q;
}

string doQuery(const Node& root, vector<string>& nodes, string attrib)
{
  int i = 0;
  Node curr = root;

  while (i < nodes.size())
  {
    for (int j = 0; j < curr.elems.size(); j++)
    {
      if (nodes[i] == curr.elems[j].name)
      {
        curr = curr.elems[j];
        i++;
        break;
      }
    }
  }

  return curr.attribs.find(attrib) != curr.attribs.end() ? curr.attribs[attrib] : "Not Found!";
}

int main()
{
  int n, q;
  cin >> n >> q;
  cin.get();

  stringstream ss;
  string temp;
  for (int i = 0; i < n; i++)
  {
    getline(cin, temp);
    ss << temp << '\n';
  }

  Node *hrml = parse(ss);

  for (int i = 0; i < q; i++)
  {
    string line;
    cin >> line;
    pair<vector<string>, string> query = parseQuery(line);
    cout << doQuery(*hrml, query.first, query.second) << endl;
  }

  return 0;
}