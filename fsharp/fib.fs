let rec fibList n =
  match n with
  | 1 -> [1I]
  | 2 -> [1I; 1I]
  | n ->
      let ys = fibList ( n - 1 )
      List.head ys + List.head ( List.tail ys ) :: ys

let rec fibTuple n =
  match n with 
  | 1 -> ( 0I, 1I )
  | 2 -> ( 1I, 1I )
  | n ->
      let p = fibTuple ( n - 1 )
      ( snd p, fst p + snd p )

let fibSeq n =
  let prev = ref 0
  let curr = ref 1
  seq { 
    for i in 1 .. n do
      let t = !prev
      prev := !curr
      curr := !prev + t
      yield !prev
  }

[<EntryPoint>]
let main args =
  let x = System.Console.ReadLine() |> int
  printfn "%A" (fibList x)
  printfn "%A" (fibTuple x)
  printfn "%A" (Seq.last (fibSeq x))
  0