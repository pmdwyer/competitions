#include <iostream>
#include <sstream>

using namespace std;

int main( int argc, char* argv[] )
{
  int n;
  stringstream ss;
  ss << "I hate ";
  cin >> n;

  for ( int i = 1; i < n; ++i )
  {
    ss << "that I ";
    if ( i & 1 ) // odd
    {
      ss << "love ";
    }
    else // even
    {
      ss << "hate ";
    }
  }

  ss << "it";
  cout << ss.str() << endl;
  return 0;
}