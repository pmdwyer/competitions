line = input().replace(';',',').split(',')

nums = []
words = []
for w in line:
    if w.isdigit():
        if int(w) > 0:
            if w[0] == '0':
                words.append(w)
            else:
                nums.append(w)
        elif int(w) == 0:
            if len(w) > 1:
                words.append(w)
            else:
                nums.append(w)
        else:
            words.append(w)    
    else:
        words.append(w)

a = '-'
if len(nums) > 0:
    a = '"{0}"'.format(",".join(nums))
b = '-'
if len(words) > 0:
    b = '"{0}"'.format(",".join(words))
print(a)
print(b)
