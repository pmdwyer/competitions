#include <iostream>

using namespace std;

int main( int argc, char* argv[] )
{
  int numProbs, travelTime, solveTime, totalTime;
  cin >> numProbs >> travelTime;

  solveTime = 240 - travelTime;
  totalTime = 5 * ( numProbs * ( numProbs + 1 ) ) / 2;
  while ( totalTime > solveTime )
  {
    numProbs--;
    totalTime = 5 * ( numProbs * ( numProbs + 1 ) ) / 2;
  }

  cout << numProbs << endl;

  return 0;
}