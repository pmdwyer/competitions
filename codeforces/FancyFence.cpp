#include <iostream>

using namespace std;

int main()
{
  int t;
  cin >> t;
  for (int i = 0; i < t; i++)
  {
    int a, b, n;
    cin >> a;
    b = 180 - a;
    n = 360 / b;
    if (b * n == 360)
    {
      cout << "YES" << endl;
    }
    else
    {
      cout << "NO" << endl;
    }
  }
}