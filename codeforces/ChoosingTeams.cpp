#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(int argc, char* argv[])
{
    int n, k;
    vector<int> members;
 
    cin >> n >> k;
    members.reserve(n);
    for (int i = 0; i < n; ++i)
    {
        int temp;
        cin >> temp;
        members.push_back(temp);
    }
    
    sort(members.begin(), members.end());
    
    int count = 0;
    for (int i = 0; i + 2 < n; i += 3)
    {
        if (members[i] + k <= 5 && members[i + 2] + k <= 5)
        {
            count++;
        }
    }
    
    cout << count << endl;
    return 0;
}
