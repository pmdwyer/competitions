#include <iostream>
#include <cctype>
#include <algorithm>

using namespace std;

int main( int argc, char* argv[] )
{
    string word;
    cin >> word;
    int numUpper = 0;
    
    for ( int i = 0; i < word.length(); ++i )
    {
        if ( isupper( word[ i ]) )
        {
            numUpper++;
        }
    }

    if ( numUpper > ( word.length() - numUpper) )
    {
        transform( word.begin(), word.end(), word.begin(), ::toupper );
    }
    else
    {
        transform( word.begin(), word.end(), word.begin(), ::tolower );
    }

    cout << word << endl;

    return 0;
}