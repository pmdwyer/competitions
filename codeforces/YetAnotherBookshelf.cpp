#include <iostream>
#include <vector>

using namespace std;

int gaps(const vector<int>& shelf)
{
	int g = 0;

	int l = -1, i = 0;
	for (; i < shelf.size() && shelf[i] == 0; i++) {}
	l = i;

	int r = l;
	while (l <= r && l < shelf.size())
	{
		int j = l + 1;
		for (; j < shelf.size() && shelf[j] == 0; j++) {}
		r = j;
		if (r == shelf.size() || l == shelf.size())
			break;
		g += (r - (l + 1));
		l = r;
	}

	return g;
}

int main()
{
	int cases = 0;
	cin >> cases;
	for (int c = 0; c < cases; c++)
	{
		int books;
		cin >> books;
		vector<int> shelf(books);
		for (int b = 0; b < books; b++)
		{
			cin >> shelf[b];
		}
		cout << gaps(shelf) << endl;
	}
	return 0;
}