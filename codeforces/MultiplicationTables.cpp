#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

using ull = unsigned long long;

int main()
{
  ull n, x, num = 0;
  cin >> n >> x;
  ull root = sqrt(x) + 1 > n ? sqrt(x) + 1 : n;
  for (ull i = 1; i <= root; i++)
  {
    if (x % i == 0 && i <= n && (x / i) <= n)
    {
      num++;
    }
  }
  cout << num << endl;
  return 0;
}