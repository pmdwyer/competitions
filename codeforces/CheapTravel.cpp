#include <iostream>

using namespace std;

int main()
{
    int n, m, a, b;
    
    cin >> n >> m >> a >> b;
    
    int cost = 0;
    int multiple = m / b;
    if (multiple < a)
    {
        if (n / m == 0)
            cost += b;
        else
        {
            cost += (n / m) * b;
            if (a < b)
            {
                cost += (n % m) * a;
            }
            else
            {
                cost += b;
            }
        }
    }
    else
    {
        cost += n * a;
    }
   
    cout << cost;
    
    return 0;
}
