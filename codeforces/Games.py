n = int(input())
home = list()
away = list()
for i in range(n):
    words = input().split()
    home.append(int(words[0]))
    away.append(int(words[1]))
count = 0
for i in range(n):
    for j in range(n):
        if i == j:
            continue
        if away[i] == home[j]:
            count += 1
print(count)
