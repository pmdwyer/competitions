#include <iostream>
#include <string>
#include <cstdlib>

#define MAX(a,b) (a > b ? a : b)

using namespace std;

int main(int argc, char* argv[])
{
    string balance;
    long long tens, hundreds, newBalance;
    cin >> balance;
    
    newBalance = atoll(balance.c_str());
    tens = newBalance / 10;
    hundreds = atoll(balance.erase(balance.length() - 2, 1).c_str())
    if (newBalance < 0)
    {
        newBalance = MAX(tens, hundreds);
    }
    
    cout << newBalance << endl;
    return 0;
}
