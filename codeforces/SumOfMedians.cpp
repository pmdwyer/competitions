#include <iostream>
#include <vector>

using namespace std;

int main()
{
  int t;
  cin >> t;
  for (; t > 0; t--)
  {
    int n, k;
    cin >> n >> k;
    vector<int> as(n*k);
    for (int i = 0; i < n*k; i++)
    {
      cin >> as[i];
    }
    int midx = ((n / 2) + (n % 2 == 0 ? 0 : 1)) - 1;
    int sum = 0;
    cout << midx << endl;
    while (midx < n * k)
    {
      cout << as[midx] << ' ';
      sum += as[midx];
      midx += n;
    }
    cout << endl;
    cout << sum << endl;
  }
  return 0;
}
