#include <iostream>
#include <string>

using namespace std;

int main()
{
  int t;
  cin >> t;
  for (; t > 0; t--)
  {
    int n, c0, c1, h;
    string s;
    cin >> n >> c0 >> c1 >> h;
    cin >> s;

    int zeros = 0, ones = 0, cost, savings;
    for (auto& c : s)
    {
      c == '0' ? zeros++ : ones++;
    }

    if (c0 < c1)
    {
      cost = zeros * c0;
      if (c1 - h > c0)
        savings = (ones * h) + (c0 * ones);
      else
        savings = ones * c1;
      cost += savings;
    }
    else if (c1 < c0)
    {
      cost = ones * c1;
      if (c0 - h > c1)
        savings = (zeros * h) + (c1 * zeros);
      else
        savings = zeros * c0;
      cost += savings;
    }
    else
    {
      cost = c0 * n;
    }
    cout << cost << endl;
  }
  return 0;
}