#include <iostream>

using namespace std;

int main (int argc, char* argv[])
{
    int n1, n2, k1, k2;
    cin >> n1 >> n2 >> k1 >> k2;
    
    int divA = n1 / k1;
    int divB = n2 / k2;
    int moveA = divA + (n1 % k1 > 0 ? 1 : 0);
    int moveB = divB + (n2 % k2 > 0 ? 1 : 0);
    cout << divA << ' ' << divB << ' ' << moveA << ' ' << moveB << endl;
    cout << (moveB < moveA ? "Second" : "First") << endl;
    return 0;
}
