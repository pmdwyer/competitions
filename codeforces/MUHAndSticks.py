lens = list(map(int, input().split()))
sticks = dict()
for i in range(len(lens)):
    if i in sticks:
        sticks[i] += 1
    else:
        sticks[i] = 0
leg = -1
head = -1
body = -1

for i in sticks.keys():
    if sticks[i] == 4:
        leg = i
    if sticks[i] == 1:
        head = i
        body = i
    if sticks[i] == 2:
        head = i
        body = i

print (sticks)

