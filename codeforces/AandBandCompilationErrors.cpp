#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
  int n;
  cin >> n;

  vector<int> as(n);
  vector<int> bs(n - 1);
  vector<int> cs(n - 2);

  for (int i = 0; i < n; i++)
  {
    cin >> as[i];
  }
  for (int i = 0; i < n - 1; i++)
  {
    cin >> bs[i];
  }
  for (int i = 0; i < n - 2; i++)
  {
    cin >> cs[i];
  }
  sort(as.begin(), as.end());
  sort(bs.begin(), bs.end());
  sort(cs.begin(), cs.end());

  int i;
  for (i = 0; i < n - 1; i++)
  {
    if (as[i] != bs[i])
    {
      cout << as[i] << endl;
      break;
    }
  }
  if (i == (n - 1))
  {
    cout << as[n - 1] << endl;
  }

  for (i = 0; i < n - 2; i++)
  {
    if (bs[i] != cs[i])
    {
      cout << bs[i] << endl;
      break;
    }
  }
  if (i == (n - 2))
  {
    cout << bs[n - 2] << endl;
  }

  return 0;
}