#include <iostream>
#include <cmath>

using namespace std;

int main()
{
  int n, days = 0, rem = 0, bacteria = 0;
  cin >> n;
  
  days = static_cast<int>(log2(n));
  n -= static_cast<int>(pow(2, days));
  bacteria++;
  while (n > 0)
  {
    days = static_cast<int>(log2(n));
    n -= static_cast<int>(pow(2, days));
    bacteria++;
  }
  cout << bacteria << endl;
  return 0;
}