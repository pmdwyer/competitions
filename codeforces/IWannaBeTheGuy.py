n = int(input())
p = list(map(int, input().split()))
q = list(map(int, input().split()))
ps = p[1:] if len(p) > 1 else []
qs = q[1:] if len(q) > 1 else []
p = p[0]
q = q[0]
levels = [False]*n
for i in ps:
    if i - 1 > -1:
        levels[i - 1] = True
for i in qs:
    if i - 1 > -1:
        levels[i - 1] = True

print("I become the guy." if all(levels) else "Oh, my keyboard!")
