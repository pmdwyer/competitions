#include <algorithm>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <bitset>

using namespace std;

typedef unsigned long long ull;

int main( int argc, char* argv[] )
{
  vector< ull > totals( 100001, 0 );
  vector< ull > memo( 100001, 0 );
  ull len, maxA = 0;

  cin >> len;
  for ( int i = 0; i < len; ++i )
  {
    ull a;
    cin >> a;
    maxA = max( maxA, a );
    totals[ a ]++;
  }

  memo[ 0 ] = 0;
  memo[ 1 ] = totals[ 1 ];

  for ( int i = 2; i <= maxA; ++i )
  {
    memo[ i ] = totals[ i ] * i + memo[ i - 2 ];
    memo[ i ] = max( memo[ i ], memo[ i - 1 ] );
  }

  cout << memo[ maxA ] << endl;
  
  return 0;
}