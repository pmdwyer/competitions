#include <iostream>
#include <vector>
#include <algorithm>
#include <ios>

using namespace std;

int main( int argc, char* argv[] )
{
    int n, l;
    cin >> n >> l;
    vector<int> as( n );

    for ( int i = 0; i < n; ++i )
    {
        cin >> as[ i ];
    }

    sort( as.begin(), as.end() );

    int maxDist = INT_MIN;
    int prev = -as[ 0 ];
    for ( int i = 0; i < n; ++i )
    {
        if ( as[ i ] - prev > maxDist )
        {
            maxDist = as[ i ] - prev;
        }
        prev = as[ i ];
    }
    if ( 2 * ( l - prev ) > maxDist )
    {
        maxDist = 2 * ( l - prev );
    }

    cout.precision( 10 );
    cout.setf( ios::fixed, ios::floatfield );
    cout << ( float ) maxDist / 2.0f << endl;

    return 0;
}