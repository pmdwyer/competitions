main :: IO ()
main = do
  greeting <- getLine
  putStrLn $ if contains "hello" greeting then "YES" else "NO"

contains :: String -> String -> Bool
contains [] _ = True
contains _ [] = False
contains (x:xs) (y:ys)
  | x == y    = contains xs ys
  | otherwise = contains (x:xs) ys
