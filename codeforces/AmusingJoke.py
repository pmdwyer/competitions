import sys
host = input()
guest = input()
pile = input()
d = dict()
for i in host:
    if i in d:
        d[i] += 1
    else:
        d[i] = 1
for i in guest:
    if i in d:
        d[i] += 1
    else:
        d[i] = 1

for i in pile:
    if i not in d:
        print("NO")
        sys.exit()
    elif i in d:
        d[i] -= 1

for i in d.values():
    if i != 0:
        print("NO")
        sys.exit()

print("YES")
