#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
  int n, m;
  cin >> n;
  vector<int> as(n);
  for (int i = 0; i < n; i++)
  {
    cin >> as[i];
  }
  cin >> m;
  vector<int> bs(m);
  for (int i = 0; i < m; i++)
  {
    cin >> bs[i];
  }

  sort(as.begin(), as.end());
  sort(bs.begin(), bs.end());

  int matches = 0;
  for (int i = 0; i < n; i++)
  {
    for (int j = 0; j < m; j++)
    {
      if (abs(as[i] - bs[j]) <= 1)
      {
        matches++;
        bs[j] = 200;
        break;
      }
    }
  }

  cout << matches << endl;

  return 0;
}