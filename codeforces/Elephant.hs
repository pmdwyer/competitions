main  = getLine
    >>= return <$> (read :: String -> Int)
    >>= print . elephant

elephant :: Int -> Int
elephant x = x `div` 5 + ( if x `mod` 5 > 0 then 1 else 0 )