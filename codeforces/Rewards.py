aes = list(map(int, input().split()))
bs = list(map(int, input().split()))
n = int(input())

numas = sum(aes)
numbs = sum(bs)

shelvesNeeded = int(numas / 5)
shelvesNeeded += 1 if numas % 5 != 0 else 0
shelvesNeeded += int(numbs / 10)
shelvesNeeded += 1 if numbs % 10 != 0 else 0

print("YES" if shelvesNeeded <= n else "NO")
