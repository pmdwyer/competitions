#include <iostream>

using namespace std;

int main( int argc, char* argv[] )
{
  int k, r;
  cin >> k >> r;

  int i = 0;
  for ( i = 0; 
    ( ( ( i * k ) - r ) % 10 != 0 ) && ( ( ( i * k ) % 10 != 0 ) || i == 0 ); 
    ++i )
  { }

  cout << i << endl;

  return 0;
}