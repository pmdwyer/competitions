#include <iostream>

#define MIN(a,b) (a < b ? a : b)
#define MAX(a,b) (a > b ? a : b)

using namespace std;

int main(int argc, char* argv[])
{
    int red, blue;
    cin >> red >> blue;
    int min = MIN(red, blue);
    int max = MAX(red, blue);
    cout << min << ' ' << (max - min) / 2 << endl;
    return 0;
}
