import Data.Set (fromList, size)

main :: IO ()
main = do
  name <- getLine
  putStrLn $ gender name

gender :: Ord a => [a] -> [Char]
gender name = if isodd then "IGNORE HIM!" else "CHAT WITH HER!"
  where isodd = odd $ size $ fromList name

