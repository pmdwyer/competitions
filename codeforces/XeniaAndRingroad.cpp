#include <iostream>

#define ULL unsigned long long

using namespace std;

int main(int argc, char* argv[])
{
    int n, m;
    cin >> n >> m;
    int current = 1, next = -1;
    ULL count = 0;
    for (int i = 0; i < m; ++i)
    {
        cin >> next;
        
        if (current < next)
        {
            count += next - current;
        }
        else if (current > next)
        {
            count += n - current + next;
        }
        // else same, so add nothing
        
        current = next;
    }
    cout << count << endl;
    return 0;
}
