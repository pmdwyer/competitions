#include <iostream>

using namespace std;

int main( int argc, char* argv[] )
{
  int n, h, min;
  cin >> n >> h;
  for ( int i = 0; i < n; ++i )
  {
    int a;
    cin >> a;
    if ( a > h )
      min++;
    min++;
  }
  cout << min << endl;
  return 0;
}