main :: IO ()
main = do
  getLine
  line <- getLine
  putStrLn $ show (pairs line)

pairs :: String -> Int
pairs []       = 0
pairs (_:[])   = 0
pairs (x:y:[]) = if x == y then 1 else 0
pairs (x:y:xs) = (if x == y then 1 else 0) + pairs (y:xs)