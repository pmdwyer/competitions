#include <iostream>
#include <algorithm>

using namespace std;

int main( int argc, char* argv[] )
{
  int len, a, b, c;
  cin >> len >> a >> b >> c;

  int maxCut = 0;
  for ( int i = 0; i * a <= len; ++i )
  {
    for ( int j = 0; j * b + i * a <= len; ++j )
    {
      if ( ( i * a + j * b - len ) % -c == 0 )
      {
        int numCuts = i + j + ( ( i * a + j * b - len ) / -c );
        maxCut = max( maxCut, numCuts );
      }
    }
  }

  cout << maxCut << endl;
  return 0;
}
