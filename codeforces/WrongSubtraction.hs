main :: IO ()
main = do
  line <- getLine
  print $ wrongSub $ map (read :: String -> Int) $ words line

wrongSub :: Integral a => [a] -> a
wrongSub [n, k]
  | k == 0          = n
  | n `mod` 10 > 0  = wrongSub [n - 1, k - 1]
  | otherwise       = wrongSub [n `div` 10, k - 1]