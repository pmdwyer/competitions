#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

int main( int argc, char* argv[] )
{
  unordered_map< string, int > db;
  int n;
  cin >> n;
  for ( int i = 0; i < n; ++i )
  {
    string s;
    cin >> s;
    if ( db.find( s ) == db.end() )
    {
      db[ s ] = 1;
      cout << "OK" << endl;
    }
    else
    {
      string temp = s + std::to_string( db[ s ] );
      db[ s ] += 1;
      cout << temp << endl;
    }
  }
  return 0;
}