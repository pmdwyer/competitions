#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{

  int n, ones = 0;
  cin >> n;
  vector<int> bs(n);

  for (int i = 0; i < n; i++)
  {
    int a;
    cin >> a;
    if (a == 0)
    {
      bs[i] = 1;
    }
    else
    {
      ones++;
      bs[i] = -1;
    }
  }

  int max_ending_here = 0, maximum = -101;
  for (int i = 0; i < n; i++)
  {
    max_ending_here += bs[i];
    max_ending_here = max(bs[i], max_ending_here);
    maximum = max(maximum, max_ending_here);
  }

  cout << maximum + ones << endl;
  return 0;
}