#include <iostream>
#include <sstream>

using namespace std;

int main( int argc, char* argv[] )
{
  int n;
  cin >> n;

  bool pair = false;
  stringstream ss;
  for ( int i = 0; i < n; ++i )
  {
    string s;
    cin >> s;
    if ( !pair && s[ 0 ] == 'O' && s[ 1 ] == 'O' )
    {
      pair = true;
      s[ 0 ] = s[ 1 ] = '+';
    }
    else if ( !pair && s[ 3 ] == 'O' && s[ 4 ] == 'O' )
    {
      pair = true;
      s[ 3 ] = s[ 4 ] = '+';
    }
    ss << s << endl;
  }
  
  if ( pair )
  {
    cout << "YES";
    cout << endl << ss.str();
  }
  else
  {
    cout << "NO";
  }
  
  return 0;
}