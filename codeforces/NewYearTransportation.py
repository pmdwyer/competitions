x = list(map(int, input().split()))
n, t = x[0] - 1, x[1] - 1
board = list(map(int, input().split()))
i = 0
while i != t and i < n:
    i = i + board[i]

print("YES" if i == t else "NO")
