#include <iostream>

using namespace std;

int apartments(int number)
{
	int digit = number % 10;
	int val = (digit - 1) * 10;
	int place = 1;
	while (number > 0)
	{
		val += place;
		place++;
		number /= 10;
	}
	return val;
}

int main()
{
	int cases = 0;
	cin >> cases;
	for (int c = 0; c < cases; c++)
	{
		int t;
		cin >> t;
		cout << apartments(t) << endl;
	}
	return 0;
}