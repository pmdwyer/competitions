#include <iostream>
#include <unordered_map>
#include <string>

using namespace std;

int main()
{
  unordered_map<string, int> hedrons = 
  {
    { "Tetrahedron", 4 },
    { "Cube", 6 },
    { "Octahedron", 8 },
    { "Dodecahedron", 12 },
    { "Icosahedron", 20 }
  };

  int n, sum = 0;
  cin >> n;
  for (int i = 0; i < n; i++)
  {
    string s;
    cin >> s;
    sum += hedrons[s];
  }
  cout << sum << endl;
  return 0;
}