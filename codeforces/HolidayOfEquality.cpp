#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
  int n;
  cin >> n;
  vector<int> as(n);
  for (int i = 0; i < n; i++)
  {
    cin >> as[i];
  }
  int m = *std::max_element(begin(as), end(as));
  int sum = 0;
  std::for_each(begin(as), end(as), [&](int v) { sum += m - v;} );
  cout << sum << endl;
  return 0;
}