import Control.Monad (replicateM)
import Data.List (transpose)

main :: IO ()
main = do
  n <- getLine
  vs <- replicateM (read n) getLine
  putStrLn $ isEquilibrium $ vecSum $ map str2Vec vs

str2Vec :: String -> [Int]
str2Vec s = map (read :: String -> Int) $ words s


vecSum :: (Eq a, Num a) => [[a]] -> Bool
vecSum xs = all (==0) $ map sum $ transpose xs

isEquilibrium :: Bool -> String
isEquilibrium True  = "YES"
isEquilibrium False = "NO"