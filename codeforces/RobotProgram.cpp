#include <iostream>

using namespace std;

int main()
{
	int t;
	cin >> t;
	for (; t > 0; t--)
	{
		int x, y;
		cin >> x >> y;
		int diff = x == y ? 0 : abs(x - y) - 1;
		cout << (x + y) + diff << endl;
	}
	return 0;
}