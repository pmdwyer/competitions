words = input().split()
n = int(words[0])
m = int(words[1])

snake = ['#'*m for i in range(n)]

for i in range(3,n,4):
    snake[i] = '#' + '.'*(m-1)

for i in range(1,n,4):
    snake[i] = '.'*(m-1) + '#'

for s in snake:
    print(s)
