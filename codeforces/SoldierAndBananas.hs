main :: IO ()
main = do
  line <- getLine
  print $ bananas $ map (read :: String -> Int) $ words line

bananas :: Integral p => [p] -> p
bananas [k, n, w] = if n >= cost then 0 else cost - n
  where cost = k * ((w * (w + 1)) `div` 2)
bananas       _ = -1