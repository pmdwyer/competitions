#include <algorithm>
#include <iostream>

using namespace std;

int main( int argc, char* argv[] )
{
  int x1, x2, x3, t1, t2, t3;
  cin >> t1 >> t2 >> t3;
  // cout << t1 << t2 << t3 << endl;

  x1 = std::min( t1, t2 );
  x1 = std::min( x1, t3 );
  x3 = std::max( t1, t2 );
  x3 = std::max( x3, t3 );
  x2 = x1 == t1 ?
        x3 == t2 ?
          t3 : t2
        :
        x1 == t2 ?
          x3 == t1 ?
            t3 : t1
          :
          x3 == t1 ?
            t2 : t1;

  // cout << x1 << x2 << x3 << endl;
  cout << ( x2 - x1 ) + ( x3 - x2 ) << endl;
  return 0;
}