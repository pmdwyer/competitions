n = int(input())
force = [0, 0, 0]
for i in range(n):
    words = input().split()
    force[0] += int(words[0])
    force[1] += int(words[1])
    force[2] += int(words[2])
print("YES" if force[0] == force[1] == force[2] == 0 else "NO")
