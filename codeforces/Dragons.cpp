#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main(int argc, char* argv[])
{
    int s, n, x, y;
    vector<pair<int, int> > dragons;
    cin >> s >> n;
    for (int i = 0; i < n; ++i)
    {
        cin >> x >> y;
        dragons.push_back(make_pair(x, y));
    }
    
    sort(dragons.begin(), dragons.end());
    
    string win = "YES";
    
    for (int i = 0; i < n; ++i)
    {
        if (s > dragons[i].first)
        {
            s += dragons[i].second;
        }
        else
        {
            win = "NO";
            break;
        }
    }
    
    cout << win << endl;
    
    return 0;
}
