n = int(input())
magnet = input()
left = True if magnet == "10" else False
groups = 1
for i in range(n-1):
	magnet = input()
	if magnet == "10" and not left:
		groups += 1
		left = not left
	elif magnet == "01" and left:
		groups += 1
		left = not left
print(groups)
