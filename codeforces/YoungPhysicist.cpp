#include <iostream>

using namespace std;

int main()
{
  int n, x = 0, y = 0, z = 0;
  cin >> n;
  for ( ; n > 0; n--)
  {
    int xp, yp, zp;
    cin >> xp >> yp >> zp;
    x += xp;
    y += yp;
    z += zp;
  }
  cout << ((x == 0 && y == 0 && z == 0) ? "YES" : "NO") << endl;
  return 0;
}