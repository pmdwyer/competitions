main :: IO ()
main = do
  line <- getLine
  print $ weight 0 $map (read :: String -> Int) $ words line

weight :: (Ord a, Num t, Num a) => t -> [a] -> t
weight i [a, b] = if a > b then i else weight (i+1) [a*3, b*2]
weight _       _= -1