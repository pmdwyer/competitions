import Data.List (sort)

main :: IO ()
main = do
  _ <- getLine
  as <- getLine
  let xs = reverse $ sort $ map (read :: String -> Int) $ words as
  print $ minimax 0 0 (sum xs) xs

minimax cnt _ _ [] = cnt
minimax cnt acc total (x:xs)
  | acc > (total - acc) = cnt
  | otherwise           = minimax (cnt+1) (acc+x) total xs

-- minimax xs = foldlWhile (<total) (+) 0 $ reverse $ sort xs
--   where total = sum xs

-- foldlWhile _ _ acc [] = acc
-- foldlWhile p f acc (x:xs)
--   | p acc = foldlWhile p f (f x acc) xs
--   | otherwise = acc