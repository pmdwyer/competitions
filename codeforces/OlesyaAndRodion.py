from math import floor, log10

n, t = map(int, input().split())
tmp = t % 10
if floor(log10(t) + 1) > n:
    print (-1)
else:
    if t == 10:
        print(1, end='')
        n = n - 1
    for i in range(n):
        print(tmp, end='')
