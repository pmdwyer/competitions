#include <iostream>
#include <string>

using namespace std;

bool can(int m, int s)
{
  return (s >= 0 && s <= 9 * m);
}

int main(void)
{
  int m, sum;
  string min = "", max = "";
  cin >> m >> sum;

  int smin = sum;
  int smax = sum;
  for (int i = 0; i < m; i++)
  {
    for (int d = 0; d < 10; d++)
    {
      if (( i > 0 || d > 0 || (m == 1 && d == 0)) && can(m - i - 1, smin - d))
      {
        smin -= d;
        min += '0' + d;
        break;
      }
    }

    for (int d = 9; d >= 0; d--)
    {
      if ((i > 0 || d > 0 || (m == 1 && d == 0)) && can(m - i - 1, smax - d))
      {
        smax -= d;
        max += '0' + d;
        break;
      }
    }
  }

  if (min.length() < m || min.empty())
  {
    min = "-1";
  }
  if (max.length() < m || max.empty())
  {
    max = "-1";
  }

  cout << min << ' ' << max << endl;
  return 0;
}