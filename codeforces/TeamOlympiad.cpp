#include <iostream>
#include <algorithm>
#include <vector>
#include <tuple>

using namespace std;

int main()
{
  int n;
  vector<int> prog_index;
  vector<int> math_index;
  vector<int> pe_index;

  cin >> n;
  vector<int> ts(n);

  for (int i = 0; i < n; i++)
  {
    cin >> ts[i];
    if (ts[i] == 1)
      prog_index.push_back(i);
    else if (ts[i] == 2)
      math_index.push_back(i);
    else
      pe_index.push_back(i);
  }

  int maxteams = std::min(std::min(prog_index.size(), math_index.size()), pe_index.size());
  cout << maxteams << endl;
  for (int i = 0; i < maxteams; i++)
  {
    cout << prog_index[i] + 1 << ' ' << math_index[i] + 1 << ' ' << pe_index[i] + 1 << endl;
  }
  return 0;
}