import Data.Char (toUpper)

main :: IO ()
main = do
  word <- getLine
  putStrLn $ capitalize word

capitalize :: String -> String
capitalize (x:xs) = (toUpper x) : xs