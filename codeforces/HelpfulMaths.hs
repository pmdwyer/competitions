import Data.List (sort, intercalate)

main :: IO ()
main = do
  line <- getLine
  putStrLn $ intercalate "+" (sort $ splitOn '+' line)

splitOn :: Char -> String -> [String]
splitOn c s =
  case s of
    [] -> []
    _  -> s' : splitOn c (dropWhile (== c) s'')
      where (s', s'') = span (/= c) s