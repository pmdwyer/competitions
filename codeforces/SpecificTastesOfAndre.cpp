#include <iostream>

using namespace std;

int main()
{
  int t;
  cin >> t;
  for (; t > 0; t--)
  {
    int n;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
      cout << "1 ";
    }
  }
  return 0;
}