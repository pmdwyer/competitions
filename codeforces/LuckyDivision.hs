main :: IO ()
main = do
  n <- getLine
  putStrLn $ if isLucky $ (read :: String -> Int) n then "YES" else "NO"

luckyNums :: [Int]
luckyNums = [4, 7, 44, 47, 74, 444, 447, 474, 477, 744, 747, 777]

isLucky :: Int -> Bool
isLucky x = any (\n -> x `mod` n == 0) luckyNums