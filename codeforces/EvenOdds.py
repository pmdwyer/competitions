words = input().split()
n = int(words[0])
k = int(words[1]) - 1

if n % 2 == 0:
    if k >= int(n / 2):
        k -= int(n / 2)
        print (2 + k*2)
    else:
        print (1 + k*2)
else:
    if k >= int(n / 2) + 1:
        k -= int(n/2) + 1
        print (2 + k*2)
    else:
        print (1 + k*2)
