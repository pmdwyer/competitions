import Control.Monad ( replicateM )

main :: IO ()
main = do
  n <- getLine
  lines <- replicateM ((read :: String -> Int) n) getLine
  print $ tram 0 0 $ map (map (read :: String -> Int) . words) lines

tram :: (Ord t, Num t) => t -> t -> [[t]] -> t
tram curr cap [[x, y]] = max cap (curr - x + y)
tram curr cap ([x, y]:xs) = tram curr' (max curr' cap) xs
  where curr' = curr - x + y