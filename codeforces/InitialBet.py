c = list(map(int, input().split()))
s = sum(c)
print(int(s / len(c)) if s % len(c) == 0 and s != 0 else -1)
