words = input().split()
n = int(words[0])
m = int(words[1])
f = list(map(int, input().split()))
f.sort()
res = 999999
for i in range(len(f) - n + 1):
    if (f[i+n-1] - f[i]) < res:
        res = (f[i+n-1] - f[i]) 
print(res)
