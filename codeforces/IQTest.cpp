#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
    int n, v[100];
    int even = 0;
    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        cin >> v[i];
        if (v[i] % 2 == 0)
        {
            even++;
        }
    }
    int idx = -1;
    for (int i = 0; i < n; ++i)
    {
        if (even == 1 && v[i] % 2 == 0)
        {
            idx = i;
        }
        else if ((n - even) == 1 && v[i] % 2 == 1)
        {
            idx = i;
        }
    }
    
    cout << idx + 1 << endl;
    return 0;
}
