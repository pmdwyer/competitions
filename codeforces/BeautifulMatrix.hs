import Data.List ( elemIndex )
import Data.Maybe ( fromJust )

main :: IO ()
main = do
  vals <- sequence [getLine, getLine, getLine, getLine, getLine]
  print $ distance $ findOne (map words vals) 0

findOne :: Num t => [[[Char]]] -> t -> (t, Int)
findOne (x:xs) idx
  | elem "1" x = (idx, fromJust $ elemIndex "1" x)
  | otherwise  = findOne xs (idx + 1)

distance :: Num a => (a, a) -> a
distance (x, y) = abs(x - 2) + abs(y - 2)