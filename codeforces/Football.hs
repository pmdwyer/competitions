main :: IO ()
main = do
  players <- getLine
  putStrLn $ if football players 0 0 >= 6 then "YES" else "NO"

football :: String -> Int -> Int -> Int
football [] curr cnt = max curr cnt
football [_] curr cnt = max curr cnt
football [x, y] curr cnt 
  | x == y    = max (curr + 1) cnt
  | otherwise = max curr cnt
football (x:y:xs) curr cnt
  | x == y    = football (y:xs) (curr+1) $ max (curr+1) cnt
  | otherwise = football (y:xs) 0 cnt