import Data.Char

main = do
  x <- getLine
  y <- getLine
  putStrLn $ show $ ord2Int $ compare (map toLower x) (map toLower y)

ord2Int :: Ordering -> Int
ord2Int GT =  1
ord2Int LT = -1
ord2Int  _ =  0