#include <iostream>
#include <iomanip>

using namespace std;

int main( int argc, char* argv[] )
{
  int n;
  double frac = 0.0f;
  cin >> n;
  for ( int i = 0; i < n; ++i )
  {
    double d;
    cin >> d;
    frac += d;
  }

  cout << fixed << setprecision( 12 ) << frac / static_cast< double >( n ) << endl;
  return 0;
}