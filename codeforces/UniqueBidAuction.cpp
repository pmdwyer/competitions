#include <algorithm>
#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

int main()
{
	int t;
	cin >> t;
	for (; t > 0; t--)
	{
		int n;
		cin >> n;
		unordered_map<int, int> counts;
		vector<int> as(n);
		for (int i = 0; i < n; i++)
		{
			cin >> as[i];
			if (counts.find(as[i]) != counts.end())
				counts[as[i]]++;
			else
				counts[as[i]] = 1;
		}

		int minuniq = 200001;
		for (auto& a : counts)
		{
			if (a.second == 1)
				minuniq = min(a.first, minuniq);
		}
		int idx = -1;
		if (minuniq < 200001)
		{
			auto it = std::find(as.begin(), as.end(), minuniq);
			idx = (it - as.begin()) + 1;
		}
		cout << idx << endl;
	}
	return 0;
}