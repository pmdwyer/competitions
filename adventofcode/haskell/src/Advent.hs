module Advent
    ( increasing, increasing3, tupleify, dir2Vector, dirReduce
    ) where

increasing :: [Int] -> Int
increasing []       = 0
increasing [x]      = 0
increasing (x:y:xs)
  | y > x           = 1 + increasing (y:xs)
  | otherwise       = increasing (y:xs)

increasing3 :: [Int] -> Int
increasing3 []            = 0
increasing3 [x]           = 0
increasing3 [x, y]        = 0
increasing3 [x, y, z]     = 0
increasing3 (x:y:z:w:xs)
  | w > x                 = 1 + increasing3 (y:z:w:xs)
  | otherwise             = increasing3 (y:z:w:xs)

tupleify :: [b] -> (b, b)
tupleify []   = error "need more than zero list members"
tupleify [x]  = error "need more than one list member"
tupleify (x:y:xs) = (x, y)

dir2Vector :: (String, String) -> (Int, Int)
dir2Vector x
  | fst x == "forward"  = (read (snd x) :: Int, 0)
  | fst x == "down"     = (0, read (snd x) :: Int)
  | otherwise           = (0, -(read (snd x) :: Int))

dirReduce :: Num c => (c, c, c) -> (c, c) -> (c, c, c)
dirReduce (d, h, a) (x, y) = (depth, hori, aim)
  where depth = d + a * x
        hori  = h + x
        aim   = a + y
