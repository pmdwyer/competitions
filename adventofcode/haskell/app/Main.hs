import System.IO
import Advent

main = do
  input <- getContents
  print $ lines input

  -- day 2 - 2
  -- input <- getContents
  -- let dirs = map (dir2Vector . tupleify . words) $ lines input
  -- print dirs
  -- let pos = foldl dirReduce (0, 0, 0) dirs
  -- print $ (\(x, y, z) -> x * y) pos