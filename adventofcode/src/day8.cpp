#include <algorithm>
#include <bitset>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

template <typename T>
void print(const vector<T>& vs) {
  for (const auto& v : vs) {
    cout << v << ' ';
  }
  cout << endl;
}

template <typename K, typename V>
void print(const unordered_map<K, V>& map) {
  for (auto itr = map.begin(); itr != map.end(); itr++) {
    cout << itr->first << ' ' << itr->second << endl;
  }
  cout << endl;
}

bitset<7> convert(string& str) {
  bitset<7> bits;
  sort(str.begin(), str.end());
  for (auto& c : str) {
    bits.set(c - 'a');
  }
  return bits;
}

int diff(const bitset<7>& a, const bitset<7>& b) {
  // cout << a << endl;
  // cout << b << endl;
  // cout << "-------" << endl;
  // cout << (a ^ b) << " = " << (a ^ b).count() << endl;
  return (int) (a ^ b).count();
}

int main() {
  int count = 0, total_sum = 0;
  string line, pipe, word;
  vector<bitset<7>> digits(10), output(4);

  getline(cin, line);
  while (!cin.fail()) {
    unordered_map<bitset<7>, int> string_to_digit;
    unordered_map<int, bitset<7>> digit_to_string;
    stringstream stream(line);
    for (int i = 0; i < 10; i++) {
      stream >> word;
      bitset<7> binary = convert(word);
      // setup default mappings
      switch (binary.count()) {
        case 2:
          string_to_digit[binary] = 1;
          digit_to_string[1] = binary;
          break;
        case 3:
          string_to_digit[binary] = 7;
          digit_to_string[7] = binary;
          break;
        case 4:
          string_to_digit[binary] = 4;
          digit_to_string[4] = binary;
          break;
        case 7:
          string_to_digit[binary] = 8;
          digit_to_string[8] = binary;
          break;
        default:
          digits[i] = convert(word);
          break;
      }
    }
    stream >> pipe;
    for (int i = 0; i < 4; i++) {
      stream >> word;
      output[i] = convert(word);
    }

    // get 9, 2
    for (auto& d : digits) {
      if (d.count() == 6) {
        auto digit_diff = diff(d, digit_to_string[4]);
        if (digit_diff == 2) {
          string_to_digit[d] = 9;
          digit_to_string[9] = d;
          d.reset();
        }
      } else if (d.count() == 5) {
        auto digit_diff = diff(d, digit_to_string[4]);
        if (digit_diff == 5) {
          string_to_digit[d] = 2;
          digit_to_string[2] = d;
          d.reset();
        }
      }
    }

    // get 6, 0
    for (auto& d : digits) {
      if (d.count() == 6) {
        auto digit_diff = diff(d, digit_to_string[7]);
        if (digit_diff == 5) {
          string_to_digit[d] = 6;
          digit_to_string[6] = d;
          d.reset();
        } else if (digit_diff == 3) {
          string_to_digit[d] = 0;
          digit_to_string[0] = d;
          d.reset();
        }
      }
    }

    // get 5
    for (auto& d : digits) {
      if (d.count() == 5) {
        auto digit_diff = diff(d, digit_to_string[6]);
        if (digit_diff == 1) {
          string_to_digit[d] = 5;
          digit_to_string[5] = d;
          d.reset();
        }
      }
    }

    // get 3
    for (const auto& d : digits) {
      if (d.count() > 0) {
        string_to_digit[d] = 3;
        digit_to_string[3] = d;
      }
    }

    // print(digits);
    // print(digit_to_string);

    int place = output.size() - 1, value = 0;
    for (const auto& o : output) {
      value += (string_to_digit[o] * ((int) pow(10, place)));
      place--;
    }
    cout << value << endl;
    total_sum += value;
    getline(cin, line);
  }
  cout << "total sum: " << total_sum << endl;
  return 0;
}