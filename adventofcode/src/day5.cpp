#include <algorithm>
#include <assert.h>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class point {
  public:
    point(int a, int b)
      : x{a}, y{b}
    {};

    int x, y;
};

ostream& operator<<(ostream& os, const point& obj) {
  os << '[' << obj.x << ", " << obj.y << ']' ;
  return os;
}

class line {
  public:
    line(int x, int y, int z, int w) 
      : a(x, y), b(z, w)
    {};

    point a, b;
};

ostream& operator<<(ostream& os, const line& obj) {
  os << obj.a << " -> " << obj.b;
  return os;
}

template <typename T>
void print(const vector<T>& vs) {
  for (const auto& v : vs) {
    cout << v << endl;
  }
  cout << endl;
}

void draw(const line& l, vector<vector<int>>& board) {
  int dX = 1, dY = 0;
  int distX = l.b.x - l.a.x;
  int distY = l.b.y - l.a.y;
  int steps = max(abs(distX), abs(distY));
  if (abs(distX) == abs(distY)) {
    if (distX < 0) {
      dX = -1;
    } else {
      dX = 1;
    }
    if (distY < 0) {
      dY = -1;
    } else {
      dY = 1;
    }
  } else if (distX == 0) {
    dX = 0;
    if (distY < 0) {
      dY = -1;
    } else {
      dY = 1;
    }
  } else if (distY == 0) {
    dY = 0;
    if (distX < 0) {
      dX = -1;
    } else {
      dX = 1;
    }
  }

  int x = l.a.x, y = l.a.y;
  for (int i = 0; i <= steps; i++) {
    assert(x < board.size());
    assert(y < board[x].size());
    board[x][y]++;
    x += dX;
    y += dY;
  }
}

int main() {
  unordered_map<int, int> overlapping;
  vector<line> lines, straight_lines;

  int a, b, c, d;
  char comma;
  string arrow;
  int maxx = 0, maxy = 0;
  cin >> a >> comma >> b >> arrow >> c >> comma >> d;
  while (!cin.fail()) {
    lines.push_back(line(a, b, c, d));
    maxx = max(maxx, max(a, c));
    maxy = max(maxy, max(b, d));
    cin >> a >> comma >> b >> arrow >> c >> comma >> d;
  }
  // copy_if(lines.begin(), lines.end(), back_inserter(straight_lines),
  //         [](const line &l) {
  //           return (l.a.x == l.b.x || l.a.y == l.b.y);
  //         });
  cout << maxx << ", " << maxy << endl;

  vector<vector<int>> board(maxy + 1, vector<int>(maxx + 1, 0));
  for (const auto& line : lines) {
    draw(line, board);
  }

  int count = 0;
  for (int i = 0; i < board.size(); i++) {
    for (int j = 0; j < board[i].size(); j++) {
      if (board[i][j] > 1) {
        count++;
      }
    }
  }
  cout << count << endl;

  return 0;
}