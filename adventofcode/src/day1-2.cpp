#include <iostream>

using namespace std;

int main() {
  int a, b, c, d, count = 0;
  cin >> a >> b >> c >> d;
  while (!cin.fail()) {
    int oldSum = a + b + c;
    int newSum = b + c + d;
    if (newSum > oldSum) count++;
    a = b;
    b = c;
    c = d;
    cin >> d;
  }
  cout << count << endl;
  return 0;
}