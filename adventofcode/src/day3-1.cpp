#include <iostream>
#include <vector>

using namespace std;

int main() {
  vector<int> ones(12, 0), zeroes(12, 0);
  string binary;
  cin >> binary;
  while (!cin.fail()) {
    for (int i = 0; i < binary.length(); i++) {
      if (binary[i] == '0') {
        zeroes[i]++;
      } else {
        ones[i]++;
      }
    }
    cin >> binary;
  }
  int gamma = 0, epsilon = 0;
  for (int i = 0; i < ones.size(); i++) {
    if (ones[i] > zeroes[i]) {
      gamma = (gamma << 1) + 1;
      epsilon = (epsilon << 1);
    } else {
      gamma = (gamma << 1);
      epsilon = (epsilon << 1) + 1;
    }
  }
  cout << gamma * epsilon << endl;
  return 0;
}