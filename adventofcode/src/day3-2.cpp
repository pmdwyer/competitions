#include <algorithm>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

string filter(const vector<string>& diags, int len, bool max) {
  int i = 0;
  vector<string> results{diags};
  while (i < len && results.size() > 1) {
    int ones = 0;
    for (auto& res : results) {
      if (res[i] == '1') {
        ones++;
      }
    }
    int zeroes = results.size() - ones;
    bool use_one = ones >= zeroes;
    vector<string> temp;
    for (auto& res : results) {
      if ((max && ((use_one && res[i] == '1') || (!use_one && res[i] == '0'))) ||
         (!max && ((use_one && res[i] == '0') || (!use_one && res[i] == '1')))) {
        temp.push_back(res);
      }
    }
    if (temp.size() == 0) {
      break;
    }
    results = temp;
    i++;
  }
  return results[0];
}

int main() {
  vector<string> diags;
  vector<int> ones(12, 0), zeroes(12, 0);

  string binary;
  cin >> binary;
  while (!cin.fail()) {
    diags.push_back(binary);
    cin >> binary;
  }

  string oxygen_gen_rating = filter(diags, 12, true);
  int oxy_rating = stoi(oxygen_gen_rating, 0, 2);
  cout << oxygen_gen_rating << endl;
  string co2_scrub_rating = filter(diags, 12, false);
  int co2_rating = stoi(co2_scrub_rating, 0, 2);
  cout << co2_scrub_rating << endl;
  cout << oxy_rating << ' ' << co2_rating << ' ' << oxy_rating * co2_rating << endl;
  return 0;
}