#include <iostream>

using namespace std;

int main() {
  int x = 0, y = 0;
  while (!cin.fail()) {
    string dir;
    int amount;
    cin >> dir;
    if (cin.fail())
      break;
    cin >> amount;
    cout << dir << ' ' << amount << endl;
    if (dir == "forward") {
      x += amount;
    } else if (dir == "down") {
      y += amount;
    } else {
      y -= amount;
    }
  }
  cout << (x * y) << endl;
  return 0;
}