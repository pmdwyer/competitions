#include <iostream>
#include <list>
#include <string>
#include <sstream>
#include <tuple>
#include <unordered_map>
#include <vector>

using namespace std;

template<typename T>
void print_boards(const vector<vector<vector<T>>>& boards) {
  for (int b = 0; b < boards.size(); b++) {
    for (int i = 0; i < boards[b].size(); i++) {
      for (int j = 0; j < boards[b][i].size(); j++) {
        cout << boards[b][i][j] << ' ';
      }
      cout << endl;
    }
    cout << endl;
  }
}

template<typename T>
void print_board(const vector<vector<vector<T>>>& boards, tuple<int, int, int> coord) {
  auto board = boards[get<0>(coord)];
  for (int i = 0; i < board.size(); i++) {
    for (int j = 0; j < board[i].size(); j++) {
      cout << board[i][j] << ' ';
    }
    cout << endl;
  }
  cout << endl;
}

template<typename T>
void print(const vector<T>& vs) {
  for (const auto& v : vs) {
    cout << v << ' ';
  }
  cout << endl;
}

int check(const vector<vector<vector<bool>>>& marked, const tuple<int, int, int>& coord) {
  auto& marked_board = marked[get<0>(coord)];
  int x = get<1>(coord), y = get<2>(coord);
  bool row = true, col = true;
  for (int j = 0; j < 5; j++) {
    row &= marked_board[x][j];
  }
  for (int i = 0; i < 5; i++) {
    col &= marked_board[i][y];
  }
  return (row || col);
}

int sum_unmarked(const vector<vector<vector<bool>>>& marked, const vector<vector<vector<int>>>& boards, int board) {
  auto& marked_board = marked[board];
  auto& num_board = boards[board];
  int sum = 0;
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      if (!marked_board[i][j]) {
        sum += num_board[i][j];
      }
    }
  }
  return sum;
}

int main() {
  int board = 0;
  string line;
  vector<int> nums;
  unordered_map<int, list<tuple<int, int, int>>> coord_lists;
  vector<vector<vector<int>>> boards;
  vector<vector<vector<bool>>> marked;

  cin >> line;
  stringstream ss(line);
  while (!ss.fail()) {
    int num;
    char c;
    ss >> num >> c;
    nums.push_back(num);
  }
  print(nums);

  while (!cin.fail()) {
    marked.push_back(vector<vector<bool>>{5, vector<bool>(5, false)});
    boards.push_back(vector<vector<int>>{5, vector<int>(5, -1)});
    for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 5; j++) {
        int num;
        cin >> num;
        if (cin.fail()) {
          break;
        }
        boards[boards.size() - 1][i][j] = num;
        coord_lists[num].push_back(make_tuple(board, i, j));
      }
    }
    board++;
  }

  tuple<int, int, int> last;
  int sum = 0;
  vector<bool> won(boards.size(), false);
  for (auto num : nums) {
    auto coords = coord_lists[num];
    for (auto coord : coords) {
      marked[get<0>(coord)][get<1>(coord)][get<2>(coord)] = true;
      if (check(marked, coord)) {
        if (!won[get<0>(coord)]) {
          sum = sum_unmarked(marked, boards, get<0>(coord));
          sum *= boards[get<0>(coord)][get<1>(coord)][get<2>(coord)];
          last = coord;
        }
        won[get<0>(coord)] = true;
      }
    }
  }

  print_board(marked, last);
  print_board(boards, last);
  cout << sum << endl;

  return 0;
}