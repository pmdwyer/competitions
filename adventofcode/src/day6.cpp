#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

template <typename T>
void print(const vector<T>& vs) {
  for (const auto& v : vs) {
    cout << v << ' ';
  }
  cout << endl;
}

int main() {
  vector<long long> fishes(9, 0);
  int num;
  char comma;
  cin >> num;
  while (!cin.fail()) {
    cin >> comma;
    fishes[num]++;
    cin >> num;
  }
  for (int i = 0; i < 256; i++) {
    // print(fishes);
    vector<long long> tempfishes(9, 0);
    for (int days = 1; days < fishes.size(); days++) {
      tempfishes[days - 1] = fishes[days];
    }
    tempfishes[6] += fishes[0]; // old fish
    tempfishes[8] = fishes[0]; // new spawn
    fishes = tempfishes;
  }
  cout << accumulate(fishes.begin(), fishes.end(), 0LL) << endl;
  return 0;
}