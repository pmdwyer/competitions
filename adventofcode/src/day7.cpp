#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

template <typename T>
void print(const vector<T>& vs) {
  for (const auto& v : vs) {
    cout << v << ' ';
  }
  cout << endl;
}

int main() {
  int num, max_pos = 0;
  char comma;
  vector<int> pos;
  cin >> num;
  while (!cin.fail()) {
    cin >> comma;
    pos.push_back(num);
    max_pos = max(max_pos, num);
    cin >> num;
  }
  vector<int> num_in_pos(max_pos + 1, 0);
  for (auto& p : pos) {
    num_in_pos[p]++;
  }
  int minfuel = INT_MAX;
  for (int i = 0; i < num_in_pos.size(); i++) {
    int total_for_pos = 0;
    for (int j = 0; j < num_in_pos.size(); j++) {
      if (j == i) {
        continue;
      }
      // total_for_pos += num_in_pos[j] * abs(j - i);
      int dist = abs(j - i);
      total_for_pos += num_in_pos[j] * (dist * (dist + 1) / 2);
    }
    minfuel = min(total_for_pos, minfuel);
  }
  print(num_in_pos);
  cout << minfuel << endl;
  return 0;
}