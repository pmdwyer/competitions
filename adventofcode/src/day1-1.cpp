#include <iostream>

using namespace std;

int main() {
  int a, b, count = 0;
  cin >> a;
  do {
    cin >> b;
    if (b > a) count++;
    a = b;
  } while (!cin.fail());
  cout << count << endl;
  return 0;
}