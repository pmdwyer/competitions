#!/bin/bash
set -euo pipefail

if [[ ! -d build ]]; then
  mkdir build
fi

pushd build
cmake -G "Visual Studio 16 2019" -A x64 ..
cmake --build .
popd