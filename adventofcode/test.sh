#!/bin/bash
set -euo pipefail

./build.sh && build/Debug/advent.exe < test/input.txt