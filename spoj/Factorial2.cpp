#include <iostream>

using namespace std;

int main()
{
  int n;
  cin >> n;

  for (int i = 0; i < n; ++i)
  {
    int f;
    cin >> f;
    int zs = 0;
    while ( f / 5 > 0)
    {
      zs += f / 5;
      f /= 5;
    }
    cout << zs << endl;
  }
  return 0;
}