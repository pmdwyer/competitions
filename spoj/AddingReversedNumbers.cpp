#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
  int n;
  cin >> n;
  for (int times = 0; times < n; ++times)
  {
    string a, b, e;
    int c, d;

    cin >> a >> b;
    reverse(a.begin(), a.end());
    reverse(b.begin(), b.end());

    c = stoi(a);
    d = stoi(b);
    e = to_string(c + d);
    reverse(e.begin(), e.end());
    cout << stoi(e) << endl;
  }
  return 0;
}