#include <iostream>

using namespace std;

int main()
{
  int n;
  bool fortytwo = false;
  while (!fortytwo)
  {
    cin >> n;
    if (n == 42)
      break;
    cout << n << endl;
  }
  return 0;
}