#include <iostream>
#include <cmath>

using namespace std;

bool is_prime(int n)
{
  if (n == 1 || n < 1)
    return false;

  if (n == 2 || n == 3 || n == 5 || n == 7 || n == 11)
    return true;

  if ((n & 1) == 0)
    return false;

  if (n % 5 == 0)
    return false;

  int limit = sqrt(n) + 1;
  for (int i = 3; i < limit; i += 6)
    // 1, 3, 5 are even
    if (n % i == 0 || n % (i + 2) == 0 || n % (i + 4) == 0)
      return false;

  return true;
}

int main()
{
  int a;
  cin >> a;
  for (int i = 0; i < a; i++)
  {
    int m, n;
    cin >> m >> n;

    for (int j = m; j <= n; j++)
    {
      if (is_prime(j))
        cout << j << endl;
    }
  }
  return 0;
}
